<?php

require_once('sites/all/modules/custom/course/course.module');

class CourseTest extends PHPUnit_Framework_TestCase {
  public function testIsRegular() {
    $node = new StdClass;

    $node->field_type['und'][0]['value'] = 'graduacao';
    $this->assertTrue(_course_is_regular($node));

    $node->field_type['und'][0]['value'] = 'extensao';
    $this->assertFalse(_course_is_regular($node));

    $node->field_type['und'][0]['value'] = 'pos';
    $this->assertTrue(_course_is_regular($node));

    $node->field_type['und'][0]['value'] = 'mestrado';
    $this->assertTrue(_course_is_regular($node));

    $node->field_type['und'][0]['value'] = 'apache';
    $this->assertFalse(_course_is_regular($node));
  }

  public function testCourseType() {
    $node = new StdClass;

    $node->field_type['und'][0]['value'] = 'graduacao';
    $this->assertEquals(_course_get_type($node), 'graduacao');

    $node->field_type['und'][0]['value'] = 'pos';
    $this->assertEquals(_course_get_type($node), 'pos');

    $node->field_type['und'][0]['value'] = '';
    $this->assertEmpty(_course_get_type($node));

    $node->field_type['und'][0] = '';
    $this->assertEmpty(_course_get_type($node));

    $node->field_type = '';
    $this->assertEmpty(_course_get_type($node));

    $node = array();

    $node['values']['field_type']['und'][0]['value'] = 'graduacao';
    $this->assertEquals(_course_get_type($node), 'graduacao');

    $node['values']['field_type']['und'][0]['value'] = 'pos';
    $this->assertEquals(_course_get_type($node), 'pos');

    $node['values']['field_type']['und'][0]['value'] = '';
    $this->assertEmpty(_course_get_type($node));

    $node['values']['field_type'] = '';
    $this->assertEmpty(_course_get_type($node));

    $node['values'] = '';
    $this->assertEmpty(_course_get_type($node));

    $node = array();

    $node['field_type']['und'][0]['value'] = 'extensao';
    $this->assertEquals(_course_get_type($node), 'extensao');

    $node['field_type']['und'][0]['value'] = 'mestrado';
    $this->assertEquals(_course_get_type($node), 'mestrado');

    $node['field_type']['und'][0]['value'] = '';
    $this->assertEmpty(_course_get_type($node));

    $this->assertEmpty(_course_get_type(''));
  }
}