<?php
require('sites/all/modules/custom/sitemap_generator/classes/SitemapGenerator.php');

class SitemapGeneratorTest extends PHPUnit_Framework_TestCase
{
    public function testValidateData()
    {
        $sitemapGenerator = new SitemapGenerator();
        
        $data = array();
        $this->assertFalse($sitemapGenerator->validateData($data));

        array_push($data, array(
            'loc' => '',
            'lastmod' => '',
            'changefreq' => '',
            'priority' => ''
        ));
        $this->assertFalse($sitemapGenerator->validateData($data));

        $data[0] =  array(
            'loc' => 'http://something',
            'lastmod' => '2016-07-22',
            'changefreq' => 'daily',
            'priority' => '1.0'
        );
        $this->assertTrue($sitemapGenerator->validateData($data));
    }

    public function testBuildXML()
    {
        $sitemapGenerator = new SitemapGenerator();
        
        $sitemapGenerator->data = array(
            array(
                'loc' => 'http://something',
                'lastmod' => '2016-07-22',
                'changefreq' => 'daily',
                'priority' => '1.0'
            )/*,
            array(
                'loc' => 'http://something2',
                'lastmod' => '2016-07-25',
                'changefreq' => 'daily',
                'priority' => '1.0'
            )*/
        );

        $expected = new DOMDocument();
        $expected->loadXML($sitemapGenerator->buildXML());

        $actual = new DOMDocument();
        $actual->loadXML('<urlset><url><loc>http://something</loc><lastmod>2016-07-22</lastmod><changefreq>daily</changefreq><priority>1.0</priority></url></urlset>');

        $this->assertEqualXMLStructure($expected->firstChild, $actual->firstChild);
    }
}