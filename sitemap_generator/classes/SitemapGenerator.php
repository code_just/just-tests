<?php

class SitemapGeneratorException extends Exception{}

class SitemapGenerator
{
	public $data = array();

	public function create($data)
	{
		if ($this->validateData($data)) {
			$this->data = $data;
			$xml = $this->buildXML();

			//file_save_data($xml, $fileUri, FILE_EXISTS_REPLACE);
			return $xml;
		} else {
			throw new SitemapGeneratorException("Sitemap Data not correct.", 1);
			
		}
	}

	public function validateData($data)
	{
		if (!is_array($data) || count($data) == 0) {
			return false;
		}

		foreach ($data as $value) {
			if (!isset($value['loc']) || !isset($value['lastmod']) || !isset($value['changefreq']) || !isset($value['priority']) || empty($value['loc']) || empty($value['lastmod'])) {
				return false;
			}
		}

		return true;
	}

	public function buildXML()
	{
		$xml = new DOMDocument('1.0', 'UTF-8');
		$urlset = $xml->createElement('urlset');
		//$urlsetNS = $xml->createAttributeNS('http://www.sitemaps.org/schemas/sitemap/0.9');
		//$urlset->appendChild($urlsetNS);

		foreach ($this->data as $data) {
			$url = $xml->createElement('url');
			
			$loc = $xml->createElement('loc', $data['loc']);
			$lastmod = $xml->createElement('lastmod', $data['lastmod']);
			$changefreq = $xml->createElement('changefreq', $data['changefreq']);
			$priority = $xml->createElement('priority', $data['priority']);
			
			$url->appendChild($loc);
			$url->appendChild($lastmod);
			$url->appendChild($changefreq);
			$url->appendChild($priority);

			$urlset->appendChild($url);
		}
		
		$xml->appendChild($urlset);

		return $xml->saveXML();
	}
}